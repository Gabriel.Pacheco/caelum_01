class Teste{

      public static void main(String[] args){

	    funcionario umFuncionario;

	    umFuncionario = new funcionario();
	    
	    umFuncionario.setNome("M");
	    umFuncionario.setDEntrada("20/10/2014");
	    umFuncionario.setSalario(2000);
	    umFuncionario.setRG("2");
	    umFuncionario.setDepartamento("B");
	    
	    System.out.println("\nFuncionario inserido com sucesso");
	    System.out.println("\nNome " + umFuncionario.getNome());
	    System.out.println("\nDepartamento " + umFuncionario.getDepartamento());
	    System.out.println("\nRG " + umFuncionario.getRG());
	    System.out.println("\nData de entrada " + umFuncionario.getDEntrada());
	    System.out.println("\nSalario " + umFuncionario.recebeAumento(10));
	    System.out.println("\nSalario Anual " + umFuncionario.calculaGanhoAnual());
	

      }

}