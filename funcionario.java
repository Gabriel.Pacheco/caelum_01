public class funcionario{

	private String dataEntrada;
	private String nome;
	private double salario;
	private String RG;
	private String departamento;
	
	public void setNome(String umNome){
	
	      nome = umNome;
	      
	}

	public String getNome(){
	
	  return nome;
	
	}
	
	public void setDEntrada(String umaDEntrada){
	
	      dataEntrada = umaDEntrada;
	      
	}

	public String getDEntrada(){
	
	  return dataEntrada;
	
	}
	
	public void setSalario(double umSalario){
	
		salario = umSalario;
	
	}
	
	public double getSalario(){
	
	  return salario;
	
	}
	
	public void setRG(String umRG){
	
		RG = umRG;
	
	}
	
	public String getRG(){
	
	  return RG;
	
	}
	
	public void setDepartamento(String umDepartamento){
	
		departamento = umDepartamento;
	
	}
	
	public String getDepartamento(){
	
	  return departamento;
	
	}
	
	public double recebeAumento(double aumento){
	
	  return salario = aumento + salario;
	
	}
	
	public double calculaGanhoAnual(){
	
	  return (salario)*12;
	
	}
	
}